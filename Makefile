.PHONY: build build-linux

build:
	go build -o tracker cmd/main.go

build-linux:
	GOOS=linux GOARCH=amd64 make build
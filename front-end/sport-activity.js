document.addEventListener("DOMContentLoaded", () => {
  const send = (formData) => {
    let newObject = {
      type: formData.exercise,
      user: formData.name,
      count: Number(formData.count),
    };

    console.log(newObject);

    fetch("http://3.65.191.120:3100/api/track-activity", {
      method: "POST",
      credentials: "include",
      headers: {
        Authorization: `Bearer ${formData.token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newObject),
    })
      .then((responce) => console.log("Сообщение отправлено..."))
      .catch((error) => console.error(error));
  };

  const form = document.getElementById("formElem");
  form.addEventListener("submit", function (e) {
    e.preventDefault();

    let formData = new FormData(this);
    formData = Object.fromEntries(formData);

    const saveDataToLocalStorage = () => {
      let convertedToJson = JSON.stringify(formData);
      localStorage.setItem("isSaved", convertedToJson);
    };

    saveDataToLocalStorage();

    send(formData);
    form.reset();
  });

  const newItputElement = document.getElementById("new-exercice");
  const saveButton = document.getElementById("btn-save-property");

  const addNewProperty = document.getElementById("add-exercise-button");
  addNewProperty.addEventListener("click", () => {
    newItputElement.style.display = "block";
    saveButton.style.display = "block";
  });

  const saveNewDataButton = document.getElementById("btn-save-property");
  saveNewDataButton.addEventListener("click", (e) => {
    console.log("save button is clicked");
    e.preventDefault();

    const newExerciceInput = document.getElementById("new-exercice").value;
    const selectExercice = document.getElementById("exercise");

    newItputElement.style.display = "";
    saveButton.style.display = "";

    let newOption = new Option(newExerciceInput);
    selectExercice.append(newOption);
    selectExercice.value = newExerciceInput;

    let selectFromLocalStorage = localStorage.getItem("newUserExercise");
    if (selectFromLocalStorage) {
      let parseSelect = JSON.parse(selectFromLocalStorage);
      parseSelect.exercises.push(selectExercice.value);
      let secondExerciseJson = JSON.stringify(parseSelect);
      localStorage.setItem("newUserExercise", secondExerciseJson);
    } else {
      const firstExercise = {
        exercises: [selectExercice.value],
      };
      let firstExerciseJson = JSON.stringify(firstExercise);
      localStorage.setItem("newUserExercise", firstExerciseJson);
    }
   
  });
  function loadSelectValue() {
    let valueFromLocalStorage = localStorage.getItem("newUserExercise");

    if (valueFromLocalStorage) {
      let parseValue = JSON.parse(valueFromLocalStorage);

      let select = document.getElementById("exercise");
      for (let item of parseValue.exercises) {
        let userOption = new Option(item);
        select.append(userOption);

     
        console.log(userOption);

      }
    }
  }
  loadSelectValue();
  const getDataFromLacalstorage = () => {
    let dataFromLocalStorage = localStorage.getItem("isSaved");

    if (dataFromLocalStorage) {
      let parsedDataFromLocalStorage = JSON.parse(dataFromLocalStorage);
      console.log(parsedDataFromLocalStorage);

      const tokenInput = document.getElementById("token_input");
      const nameInput = document.getElementById("name");
      const exerciseSelect = document.getElementById("exercise");
      const countInput = document.getElementById("count");

      tokenInput.value = parsedDataFromLocalStorage.token;
      nameInput.value = parsedDataFromLocalStorage.name;
      exerciseSelect.value = parsedDataFromLocalStorage.exercise;
      countInput.value = parsedDataFromLocalStorage.count;
    }
  };
  getDataFromLacalstorage();
});

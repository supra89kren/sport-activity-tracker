package main

import (
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	http2 "gitlab.com/supra89kren/sport-activity-tracker/pkg/http"
	"net/http"
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})
	cfg := getConfig()
	s, err := createServer(cfg)
	if err != nil {
		log.WithError(err).Error("error while creating the server")
	}

	log.Info(fmt.Sprintf("Starting the server on port %v", s.Addr))
	err = s.ListenAndServe()
	if err != nil {
		log.WithError(err).Error("error while running the server")
	}
}

func getConfig() config {
	cfg := config{}
	flag.IntVar(&cfg.HTTPListenPort, "server.http-listen-port", 3100, "HTTP server listen port.")
	flag.Parse()
	return cfg
}

func createServer(cfg config) (*http.Server, error) {
	router := mux.NewRouter()
	router.Path("/metrics").Handler(promhttp.Handler())
	router.Use(mux.CORSMethodMiddleware(router))
	router.Use(http2.CorsMiddlware())
	router.Use(http2.AuthMiddleware())
	router.Handle("/api/track-activity", &http2.ActivityTrackingServer{}).Methods(http.MethodPost, http.MethodOptions)
	return &http.Server{
		Handler: router,
		Addr:    fmt.Sprintf(":%d", cfg.HTTPListenPort),
	}, nil
}

type config struct {
	HTTPListenPort int
}

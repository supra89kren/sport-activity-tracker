package http

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strings"
)

var (
	activityCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "sport_activities_total",
		Help: "The total number of sport activities reported",
	},
		[]string{"type", "user"},
	)
)

type ActivityTrackingServer struct {
}

func (a *ActivityTrackingServer) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		writeError(writer, "can not read request body", err, http.StatusBadRequest)
		return
	}
	requestBody := &TrackActivityRequest{}
	err = json.Unmarshal(body, requestBody)
	if err != nil {
		writeError(writer, "can not parse request body", err, http.StatusBadRequest)
		return
	}

	log.WithField("user", requestBody.User).WithField("type", requestBody.ActivityType).WithField("user", requestBody.User).Info("received activity")
	activityCounter.WithLabelValues(requestBody.ActivityType, requestBody.User).Add(float64(requestBody.Count))
	writer.WriteHeader(http.StatusOK)
}

func writeError(writer http.ResponseWriter, message string, err error, statusCode int) {
	log.WithError(err).Error(message)
	writer.WriteHeader(statusCode)
	data, err := json.Marshal(&errorResponse{message})
	if err != nil {
		log.WithError(err).Error("can not marshal error")
	} else {
		_, _ = writer.Write(data)
	}
}

type TrackActivityRequest struct {
	ActivityType string `json:"type"`
	User         string `json:"user"`
	Count        int    `json:"count"`
}

func AuthMiddleware() mux.MiddlewareFunc {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			if request.RequestURI == "/metrics" {
				handler.ServeHTTP(writer, request)
				return
			}
			responseError := validateAuthHeader(request.Header.Get("Authorization"))
			if responseError != nil {
				log.WithField("reason", responseError.Message).Warn("authentication failed")
				writer.WriteHeader(http.StatusUnauthorized)
				data, err := json.Marshal(responseError)
				if err != nil {
					log.WithError(err).Error("can not marshal error")
				} else {
					_, err = writer.Write(data)
					if err != nil {
						log.WithError(err).Error("can not write to the client")
					}
				}
				return
			}
			handler.ServeHTTP(writer, request)
		})
	}
}

func validateAuthHeader(authorizationHeader string) *errorResponse {
	if len(authorizationHeader) == 0 {
		return &errorResponse{"authorization header is not defined"}
	}
	if !strings.HasPrefix(authorizationHeader, "Bearer ") {
		return &errorResponse{"auth token must start with 'Bearer '"}
	}
	if "86e592fc-da56-4349-966a-cd19cd0d8d40" != authorizationHeader[7:] {
		return &errorResponse{"wrong auth secret"}
	}
	return nil
}

type errorResponse struct {
	Message string `json:"message"`
}

func CorsMiddlware() mux.MiddlewareFunc {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
			w.Header().Set("Access-Control-Allow-Headers", "Content-Type,Authorization")
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			if r.Method == http.MethodOptions {
				w.WriteHeader(200)
				return
			}
			h.ServeHTTP(w, r)
		})
	}
}
